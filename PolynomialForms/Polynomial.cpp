//
// Created by Александр Баташев on 18.03.2018.
//

#include "Polynomial.h"

Polynomial::Polynomial(std::string polynomialStr, int maxDeg, int varCount) {
    this->polynomialStr = polynomialStr;
    this->maxDeg = maxDeg;
    this->varCount = varCount;

    this->list = new LoopList();

    int count = 0;

    std::string *words = new std::string[this->polynomialStr.size() + 2];
    stringToWords(this->polynomialStr, words, count);

    for (int i = 0; i < count; i++) {
        this->list->add(
                stringToCoeff(words[i]),
                stringToConvolution(words[i])
        );
    }
}

Polynomial::Polynomial(const Polynomial & src) {
    this->polynomialStr = src.polynomialStr;
    this->maxDeg = src.maxDeg;
    this->varCount = src.varCount;

    this->list = new LoopList(*src.list);
}

Polynomial &Polynomial::operator=(const Polynomial &src) {
    this->polynomialStr = src.polynomialStr;
    this->maxDeg = src.maxDeg;
    this->varCount = src.varCount;

    delete list;

    this->list = new LoopList(*src.list);
    return *this;
}

Polynomial::~Polynomial() {
    delete list;
}

Polynomial Polynomial::operator+(const Polynomial &rhs) {
    Polynomial res;

    if (maxDeg == rhs.maxDeg && varCount == rhs.varCount) {
        res = *this;
        *res.list = (*res.list) + (*rhs.list);
    }

    return res;
}

Polynomial Polynomial::operator-(const Polynomial &rhs) {
    Polynomial res;

    if (maxDeg == rhs.maxDeg && varCount == rhs.varCount) {
        res = *this;
        *res.list = (*res.list) - (*rhs.list);
    }

    return res;
}

Polynomial Polynomial::operator*(const Polynomial &rhs) {
    Polynomial res;

    if (maxDeg == rhs.maxDeg && varCount == rhs.varCount) {
        res = *this;
        *res.list = res.list->multiply(*rhs.list, maxDeg, varCount);
    }

    return res;
}

void Polynomial::addMonom(std::string monomStr) {
    this->list->add(
            stringToCoeff(monomStr),
            stringToConvolution(monomStr)
    );
}

std::string Polynomial::toString() {
    return this->list->toString(maxDeg, varCount);
}

void Polynomial::stringToWords(std::string str, std::string *words, int &size) {

    std::string delimiters = "+-* ";
    std::string tmp = str + " ";
    size = 0;
	for (int i = 0; i < tmp.length(); i++) {
        if (delimiters.find(tmp[i]) == std::string::npos) {
            words[size] += tmp[i];
        } else {
            if (tmp[i] == ' ') {
                if (!words[size].empty()) {
                    words[++size] = "";
                }
            }
            else {
                words[size] += tmp[i];
            }
        }
    }

}

int Polynomial::stringToCoeff(std::string str) {
	if (str.empty()) {
		return 0;
	}

	int start = 0;

	if (str[0] == '-') {
		start = 1;
	}

	if (str[start] == 'x') {
		return start == 0 ? 1 : -1;
	}

	if (str[start] >= '0' && str[start] <= '9') {
		std::string num;

		for (int i = start; i < str.length() && str[i] >= '0' && str[i] <= '9'; i++) {
			num += str[i];
		}

		int res = std::stoi(num);

		if (start == 1) {
			res *= -1;
		}

		return res;
	}

	return 0;
}

int Polynomial::stringToConvolution(std::string str) {
	int *ni = new int[varCount];

	for (int i = varCount; i > 0; i--) {
		std::string t = "x" + std::to_string(i);
		unsigned long pos = str.find(t);
		if (pos == std::string::npos) {
			ni[i - 1] = 0;
		}
		else {
			str.erase(pos, t.length());

			if ((pos < str.length() && str[pos] == 'x') || pos >= str.length()) {
				ni[i - 1] = 1;
			}
			else if (pos < str.length() && str[pos] == '^') {
				str.erase(pos, 1);
				std::string num = "";

				while (pos < str.length()
					&& (str[pos] >= '0' && str[pos] <= '9')) {
					num += str[pos];
					str.erase(pos, 1);
				}

				ni[i - 1] = std::stoi(num);

				if (ni[i - 1] > maxDeg) {
					return -1;
				}
			}
		}
	}

	int s = 0;

	for (int i = 0; i < varCount; i++) {
		s = s * maxDeg + ni[i];
	}

	return s;
}

