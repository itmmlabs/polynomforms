#pragma once

#include "Polynomial.h"
#include <string>
#include <msclr\marshal_cppstd.h>

namespace PolynomialForms {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace std;
	using namespace System::Runtime::InteropServices;

	/// <summary>
	/// ������ ��� MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
			maxDeg = 10;
			varCount = 3;
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  setPolynom1;
	protected:
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  pol1Box;
	private: System::Windows::Forms::TextBox^  monom1;


	private: System::Windows::Forms::Button^  addMonom1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  pol2Box;

	private: System::Windows::Forms::Button^  setPolynom2;
	private: System::Windows::Forms::Button^  addMonom2;
	private: System::Windows::Forms::TextBox^  monom2;

	private: System::Windows::Forms::Button^  sumBtn;
	private: System::Windows::Forms::Button^  difBtn;
	private: System::Windows::Forms::Button^  mulBtn;
	private: System::Windows::Forms::TextBox^  result;

			 Polynomial *pol1;
			 Polynomial *pol2;
			 int maxDeg;
			 int varCount;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  degBox;
	private: System::Windows::Forms::TextBox^  vcBox;


	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Button^  button1;

			 

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->setPolynom1 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->pol1Box = (gcnew System::Windows::Forms::TextBox());
			this->monom1 = (gcnew System::Windows::Forms::TextBox());
			this->addMonom1 = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->pol2Box = (gcnew System::Windows::Forms::TextBox());
			this->setPolynom2 = (gcnew System::Windows::Forms::Button());
			this->addMonom2 = (gcnew System::Windows::Forms::Button());
			this->monom2 = (gcnew System::Windows::Forms::TextBox());
			this->sumBtn = (gcnew System::Windows::Forms::Button());
			this->difBtn = (gcnew System::Windows::Forms::Button());
			this->mulBtn = (gcnew System::Windows::Forms::Button());
			this->result = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->degBox = (gcnew System::Windows::Forms::TextBox());
			this->vcBox = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// setPolynom1
			// 
			this->setPolynom1->Location = System::Drawing::Point(565, 64);
			this->setPolynom1->Name = L"setPolynom1";
			this->setPolynom1->Size = System::Drawing::Size(75, 23);
			this->setPolynom1->TabIndex = 0;
			this->setPolynom1->Text = L"Set";
			this->setPolynom1->UseVisualStyleBackColor = true;
			this->setPolynom1->Click += gcnew System::EventHandler(this, &MyForm::setPolynom1_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(9, 49);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(56, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Polynom 1";
			// 
			// pol1Box
			// 
			this->pol1Box->Location = System::Drawing::Point(12, 66);
			this->pol1Box->Name = L"pol1Box";
			this->pol1Box->Size = System::Drawing::Size(547, 20);
			this->pol1Box->TabIndex = 2;
			// 
			// monom1
			// 
			this->monom1->Location = System::Drawing::Point(12, 96);
			this->monom1->Name = L"monom1";
			this->monom1->Size = System::Drawing::Size(547, 20);
			this->monom1->TabIndex = 3;
			// 
			// addMonom1
			// 
			this->addMonom1->Location = System::Drawing::Point(565, 93);
			this->addMonom1->Name = L"addMonom1";
			this->addMonom1->Size = System::Drawing::Size(75, 23);
			this->addMonom1->TabIndex = 4;
			this->addMonom1->Text = L"Add monom";
			this->addMonom1->UseVisualStyleBackColor = true;
			this->addMonom1->Click += gcnew System::EventHandler(this, &MyForm::addMonom1_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 140);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(56, 13);
			this->label2->TabIndex = 5;
			this->label2->Text = L"Polynom 2";
			// 
			// pol2Box
			// 
			this->pol2Box->Location = System::Drawing::Point(12, 157);
			this->pol2Box->Name = L"pol2Box";
			this->pol2Box->Size = System::Drawing::Size(547, 20);
			this->pol2Box->TabIndex = 6;
			// 
			// setPolynom2
			// 
			this->setPolynom2->Location = System::Drawing::Point(565, 155);
			this->setPolynom2->Name = L"setPolynom2";
			this->setPolynom2->Size = System::Drawing::Size(75, 23);
			this->setPolynom2->TabIndex = 7;
			this->setPolynom2->Text = L"Set";
			this->setPolynom2->UseVisualStyleBackColor = true;
			this->setPolynom2->Click += gcnew System::EventHandler(this, &MyForm::setPolynom2_Click);
			// 
			// addMonom2
			// 
			this->addMonom2->Location = System::Drawing::Point(565, 182);
			this->addMonom2->Name = L"addMonom2";
			this->addMonom2->Size = System::Drawing::Size(75, 23);
			this->addMonom2->TabIndex = 8;
			this->addMonom2->Text = L"Add monom";
			this->addMonom2->UseVisualStyleBackColor = true;
			this->addMonom2->Click += gcnew System::EventHandler(this, &MyForm::addMonom2_Click);
			// 
			// monom2
			// 
			this->monom2->Location = System::Drawing::Point(12, 184);
			this->monom2->Name = L"monom2";
			this->monom2->Size = System::Drawing::Size(547, 20);
			this->monom2->TabIndex = 9;
			// 
			// sumBtn
			// 
			this->sumBtn->Location = System::Drawing::Point(710, 93);
			this->sumBtn->Name = L"sumBtn";
			this->sumBtn->Size = System::Drawing::Size(75, 23);
			this->sumBtn->TabIndex = 11;
			this->sumBtn->Text = L"+";
			this->sumBtn->UseVisualStyleBackColor = true;
			this->sumBtn->Click += gcnew System::EventHandler(this, &MyForm::sumBtn_Click);
			// 
			// difBtn
			// 
			this->difBtn->Location = System::Drawing::Point(710, 123);
			this->difBtn->Name = L"difBtn";
			this->difBtn->Size = System::Drawing::Size(75, 23);
			this->difBtn->TabIndex = 12;
			this->difBtn->Text = L"-";
			this->difBtn->UseVisualStyleBackColor = true;
			this->difBtn->Click += gcnew System::EventHandler(this, &MyForm::difBtn_Click);
			// 
			// mulBtn
			// 
			this->mulBtn->Location = System::Drawing::Point(710, 153);
			this->mulBtn->Name = L"mulBtn";
			this->mulBtn->Size = System::Drawing::Size(75, 23);
			this->mulBtn->TabIndex = 13;
			this->mulBtn->Text = L"*";
			this->mulBtn->UseVisualStyleBackColor = true;
			this->mulBtn->Click += gcnew System::EventHandler(this, &MyForm::mulBtn_Click);
			// 
			// result
			// 
			this->result->Location = System::Drawing::Point(12, 252);
			this->result->Name = L"result";
			this->result->Size = System::Drawing::Size(547, 20);
			this->result->TabIndex = 14;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(9, 20);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(35, 13);
			this->label3->TabIndex = 15;
			this->label3->Text = L"label3";
			// 
			// degBox
			// 
			this->degBox->Location = System::Drawing::Point(57, 13);
			this->degBox->Name = L"degBox";
			this->degBox->Size = System::Drawing::Size(100, 20);
			this->degBox->TabIndex = 16;
			// 
			// vcBox
			// 
			this->vcBox->Location = System::Drawing::Point(227, 13);
			this->vcBox->Name = L"vcBox";
			this->vcBox->Size = System::Drawing::Size(100, 20);
			this->vcBox->TabIndex = 17;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(186, 20);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(35, 13);
			this->label4->TabIndex = 18;
			this->label4->Text = L"label4";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(344, 9);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 19;
			this->button1->Text = L"Ok";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(811, 282);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->vcBox);
			this->Controls->Add(this->degBox);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->result);
			this->Controls->Add(this->mulBtn);
			this->Controls->Add(this->difBtn);
			this->Controls->Add(this->sumBtn);
			this->Controls->Add(this->monom2);
			this->Controls->Add(this->addMonom2);
			this->Controls->Add(this->setPolynom2);
			this->Controls->Add(this->pol2Box);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->addMonom1);
			this->Controls->Add(this->monom1);
			this->Controls->Add(this->pol1Box);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->setPolynom1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void setPolynom1_Click(System::Object^  sender, System::EventArgs^  e) {
		pol1 = new Polynomial(msclr::interop::marshal_as<std::string>(this->pol1Box->Text), maxDeg, varCount);
	}
	private: System::Void setPolynom2_Click(System::Object^  sender, System::EventArgs^  e) {
		pol2 = new Polynomial(msclr::interop::marshal_as<std::string>(this->pol2Box->Text), maxDeg, varCount);
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		maxDeg = stoi(msclr::interop::marshal_as<std::string>(this->degBox->Text));
		varCount = stoi(msclr::interop::marshal_as<std::string>(this->vcBox->Text));
		this->button1->Enabled = false;
	}
	private: System::Void sumBtn_Click(System::Object^  sender, System::EventArgs^  e) {
		Polynomial rPol = (*pol1) + (*pol2);
		this->result->Text = msclr::interop::marshal_as<System::String^>((rPol.toString()));
	}
	private: System::Void difBtn_Click(System::Object^  sender, System::EventArgs^  e) {
		Polynomial rPol = (*pol1) - (*pol2);
		this->result->Text = msclr::interop::marshal_as<System::String^>((rPol.toString()));
	}
	private: System::Void mulBtn_Click(System::Object^  sender, System::EventArgs^  e) {
		Polynomial rPol = (*pol1) * (*pol2);
		this->result->Text = msclr::interop::marshal_as<System::String^>((rPol.toString()));
	}
	private: System::Void addMonom1_Click(System::Object^  sender, System::EventArgs^  e) {
		if (pol1 != nullptr) {
			pol1->addMonom(msclr::interop::marshal_as<std::string>(this->monom1->Text));
			this->monom1->Text = L"";
			this->pol1Box->Text = msclr::interop::marshal_as<System::String^>(pol1->toString());
		}
	}
	private: System::Void addMonom2_Click(System::Object^  sender, System::EventArgs^  e) {
		if (pol2 != nullptr) {
			pol2->addMonom(msclr::interop::marshal_as<std::string>(this->monom2->Text));
			this->monom2->Text = L"";
			this->pol2Box->Text = msclr::interop::marshal_as<System::String^>(pol2->toString());
		}
	}
};
}
